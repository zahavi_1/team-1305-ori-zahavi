import socket
import ai_process
from _thread import *
import threading

CAR_AUTH_MESSAGE = "hello from esp32"
#server's socket vairiables
IP = "0.0.0.0"
PORT = 1221
server_info = (IP, PORT)
IMAGE_PATH = "images//input_images//image.jpg"

#initializing condition variables for queues
added_to_cars = threading.Condition()
added_to_guis = threading.Condition()
gui_messages_queue = []
car_messages_queue = []

class GUIhandler:
	def __init__(self, socket):
		self._socket = socket
		
		
	def handleMessage(self):
		global added_to_guis
		global gui_messages_queue

		
		message = getMessage(self._socket)
		print("Message from GUI: ", message)

		
		with added_to_guis:
			try:
				gui_messages_queue.append(message)
			except Exception as e:
				print("Found here", e)
			added_to_guis.notifyAll()
			


	def send_message(self):
		global car_messages_queue
		global added_to_cars
		with added_to_cars:
			print("Gui handler -> waiting for message to deliver")

			added_to_cars.wait()			
			message = car_messages_queue.pop(0)
			
		print("Sending", message, "to gui")
		self._socket.sendall(message.encode())			
	
	def handleGUI(self):
		while True:
			
			try:
				self.handleMessage()
			except Exception as e:
				print("Error with handleGUI", e)
			try:	
				self.send_message()
			except Exception as e:
				print("Error with send_message", e)


class CarHandler:
	force_command = False
	command = ""
	def __init__(self, socket):
		self._socket = socket
		
	def handleMessage(self):
		global car_messages_queue
		global added_to_cars
		#image arrives in format of:
		# Size -> Confirm
		#Image -> Confirm

		#get image size
		message = getMessage(self._socket)
		size = message.split()[1]
		self._socket.sendall("Got Size".encode())


		#get image
		message = getMessage(self._socket, 40960000)
		with open(IMAGE_PATH, 'wb') as f:
			f.write(message)
		self._socket.sendall("Got Image".encode())

		#operate the ai models on the image to detect and recognize road signs
		road_signs_detected = ai_process.detect_object(IMAGE_PATH)
		message_to_gui = ai_process.getClassName(road_signs_detected[0])
		
		with added_to_cars:
			car_messages_queue.append(message_to_gui)
			added_to_cars.notifyAll()
		
		if len(message_to_gui) > 1:
			force_command = True
			command = message_to_gui
	
	def send_message(self):
		global gui_messages_queue
		global added_to_guis
		with added_to_guis:
			print("Car handler -> waiting for message to deilver")
			added_to_guis.wait()
			
			if not force_command:
				try:
					message = gui_messages_queue.pop(0)
					print("Sending", message, "to car")
				except Exception as e:
					print(e, "in sending message")
			else:
				message = command
				force_command = False
		self._socket.sendall(message.encode())
		

	def handleCar(self):
		while True:
			try:
				self.send_message()
			except Exception as e:
				print("Error with sending messages", e)

			try:
				self.handleMessage()
			except Exception as e:
				print("Error in handling")
			
	
	
	
def main():
	#create and accept clients for conversation
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind(server_info)
	s.listen(0)
	while True:
		try:
			sock, addr = s.accept()
			
			#let the threads handle the rest of the work with each of them
			start_new_thread(threaded, (sock,))
		except Exception as e:
			print("Error in connecting" )
	
	
def getMessage(socket, buffer_size = 1024):
	client_message = socket.recv(buffer_size)
	client_message = client_message.decode()
	return client_message
	
def confirm_car_connection(socket):
	confirm_message = socket.recv(1024)
	confirm_message = confirm_message.decode()
	
	if confirm_message.lower() == CAR_AUTH_MESSAGE:
		return True
		
	return False
	
def threaded(socket):
	isCar=True
	try:
		
		print("Client accepted")
		if confirm_car_connection(socket):
			try:
				print("Car connected")
				#handle this thread as car thread
				handler = CarHandler(socket)
			except Exception as e:
				print("In creating handler")
			try:
				handler.handleCar()
			except Exception as e:
				print("In use handler")
				print(e)
			isCar=True
		
		else:
			isCar=False
			#handle this thread as gui thread
			print("Gui connnected")
			handler = GUIhandler(socket)
			handler.handleGUI()
			
		
	#connection closed		
		socket.close()
	except Exception as e:
		if isCar:
			print("Error with car")
			print(e)
		else:
			print("Error with gui")
		socket.close()

	
if __name__ == "__main__":
	main()

