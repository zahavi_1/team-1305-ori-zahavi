

#Import Relevant Libraries
import numpy as np
import cv2
import time
import os
from tensorflow import keras
from keras.models import load_model
import io
import PIL as Image

def getClassName(classNo):
  if   classNo == 0: return 'Stop'
  elif classNo == 1: return 'Speed limit (20km/h)'
  elif classNo == 2: return 'Speed Limit 30 km/h'
  elif classNo == 3: return 'Speed Limit 50 km/h'
  elif classNo == 4: return 'Speed Limit 80 km/h'
  elif classNo == 5: return 'No Passing'
  elif classNo == 6: return 'Yield'
  elif classNo == 7: return 'No Entry'
  elif classNo == 8: return 'End of all speed limits'
  elif classNo == 9: return 'Turn right'
  elif classNo == 10: return 'Turn left'
  elif classNo == 11: return 'Ahead only'
  elif classNo == 12: return 'General caution'

def load_images_from_folder(folder):
	images = []
	for filename in os.listdir(folder):
		img = cv2.imread(os.path.join(folder,filename))
		if img is not None:
			images.append(img)
	return images

def get_image(path):
	return cv2.imread(path)

def grayscale(img):
  img = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
  return img
def equalize(img):
  img =cv2.equalizeHist(img)
  return img
def preprocessing(img):
  img = grayscale(img)
  img = equalize(img)
  img = img/255
  return img


road_signs_counter = 0

def detect_object(path):
	global road_signs_counter
	output_folder_path = "images"
	HEIGHT = 32
	WIDTH = 32

	net = cv2.dnn.readNet("Ai-Weights/yolov3_training_last.weights", "Ai-Weights/yolov3_training.cfg")
	
	detected_signs = []

	classes = []
	with open("Ai-Weights/signs.names.txt", "r") as f:
		classes = [line.strip() for line in f.readlines()]

	#get last layers names
	layer_names = net.getLayerNames()
	output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]
	colors = np.random.uniform(0, 255, size=(len(classes), 3))
	check_time = True
	confidence_threshold = 0.5
	font = cv2.FONT_HERSHEY_SIMPLEX



	detection_confidence = 0.5
	font = cv2.FONT_HERSHEY_SIMPLEX

	classification_model = load_model("Ai-Weights/my_model.h5") #load mask detection model
	classes_classification = []
	with open("Ai-Weights/signs_classes.txt", "r") as f:
		classes_classification = [line.strip() for line in f.readlines()]

	
	img = get_image(path)
	#get image shape
	height, width, channels = img.shape
	#print(img.shape)



	# Detecting objects (YOLO)
	blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
	net.setInput(blob)
	outs = net.forward(output_layers)

	# Showing informations on the screen (YOLO)
	class_ids = []
	confidences = []
	boxes = []
	for out in outs:
		for detection in out:
			scores = detection[5:]
			class_id = np.argmax(scores)
			confidence = scores[class_id]
			if confidence > confidence_threshold:
				# Object detected
				center_x = int(detection[0] * width)
				center_y = int(detection[1] * height)
				w = int(detection[2] * width)
				h = int(detection[3] * height)
				# Rectangle coordinates
				x = int(center_x - w / 2)
				y = int(center_y - h / 2)
				boxes.append([x, y, w, h])
				confidences.append(float(confidence))
				class_ids.append(class_id)
	indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)
	for i in range(len(boxes)):
		if i in indexes:
			x, y, w, h = boxes[i]
			label = str(classes[class_ids[i]]) + "=" + str(round(confidences[i]*100, 2)) + "%"
			img = cv2.rectangle(img, (x, y), (x + w, y + h), (255,0,0), 2)
			crop_img = img[y:y+h, x:x+w]
			if len(crop_img) > 0 :
				crop_img = cv2.resize(crop_img, (32, 32))
				crop_img = preprocessing(crop_img)
				crop_img = crop_img.reshape(1, 32, 32, 1)
				prediction = np.argmax(classification_model.predict(crop_img))
				print(prediction)
				detected_signs.append(prediction)
				label = getClassName(prediction)
				img = cv2.putText(img, label, (x, y), font, 0.5, (255,0,0), 2)

	road_signs_coutner += 1
	output_folder_path = str(output_folder_path)+'/'+ str(road_signs_coutner) + '.jpg'
	cv2.imwrite(output_folder_path, img)
	
	
	return detected_signs
