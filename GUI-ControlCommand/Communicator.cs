﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;


static class Defines
{
    public const string IP = "127.0.0.1";
    public const int PORT = 1221;
}
public struct ConnectionDetails
{
    public string serverIp;
    public int serverPort;
}
namespace GUI_ControlCommand
{
    public class Communicator
    {
        private ConnectionDetails _clientDets;
        private TcpClient _client;
        private NetworkStream _clientStream;

        public Communicator(ConnectionDetails cd)
        {
            _client = new TcpClient();
            IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Parse(cd.serverIp), cd.serverPort);
            _client.Connect(serverEndPoint);
        }

        public ConnectionDetails GetConnectionDetails()
        {
            return this._clientDets;
        }

        public bool SendMessage(string messageToSend, int code)
        {
            try
            {
                string message = "";
                message += (char)code + messageToSend;

                _clientStream  =_client.GetStream();
                byte[] buffer = new ASCIIEncoding().GetBytes(message);
                _clientStream.Write(buffer, 0, buffer.Length);
                _clientStream.Flush();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public string GetResponse()
        {
            try
            {
                byte[] buffer = new byte[4096];
                int bytesRead = _clientStream.Read(buffer, 0, 4096);
                string response = System.Text.Encoding.ASCII.GetString(buffer, 0, bytesRead);

                return response;
            }
            catch(Exception e)
            {
                return "-1";
            }
        }
    }
}
