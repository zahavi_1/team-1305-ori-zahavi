﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GUI_ControlCommand
{
    public partial class ConnectionWindow : Window
    {
        private Communicator _client;
        public ConnectionWindow(Communicator client)
        {
            this._client = client;
            InitializeComponent();
        }
        
        private void forBut_Click(object sender, RoutedEventArgs e) //forward button function
        {
            string serverResponse;

            if ((int)SpeedSlider.Value > 0)
            {
                forBut.Background = Brushes.Green;
                rigBut.Background = Brushes.White;
                revBut.Background = Brushes.White;
                lefBut.Background = Brushes.White;
                stoBut.Background = Brushes.White;
            }
                _client.SendMessage("f", (int)SpeedSlider.Value);
                serverResponse = _client.GetResponse();

                AnalyzeMessage(serverResponse);
            }

        private void rigBut_Click(object sender, RoutedEventArgs e) //right button function
        {
            string serverResponse;
            
            if ((int)SpeedSlider.Value > 0)
            {
                forBut.Background = Brushes.White;
                rigBut.Background = Brushes.Green;
                revBut.Background = Brushes.White;
                lefBut.Background = Brushes.White;
                stoBut.Background = Brushes.White;
                
            }

            _client.SendMessage("r", (int)SpeedSlider.Value);
            serverResponse = _client.GetResponse();

            AnalyzeMessage(serverResponse);
        }

        private void revBut_Click(object sender, RoutedEventArgs e) //reverse button function
        {
            string serverResponse;

            if ((int)SpeedSlider.Value > 0)
            {
                forBut.Background = Brushes.White;
                rigBut.Background = Brushes.White;
                revBut.Background = Brushes.Green;
                lefBut.Background = Brushes.White;
                stoBut.Background = Brushes.White;
            }
            
            _client.SendMessage("b", (int)SpeedSlider.Value);

            serverResponse = _client.GetResponse();

            AnalyzeMessage(serverResponse);
        }

        private void lefBut_Click(object sender, RoutedEventArgs e) //left button function 
        {
            string serverResponse;

            if ((int)SpeedSlider.Value > 0)
            {
                forBut.Background = Brushes.White;
                rigBut.Background = Brushes.White;
                revBut.Background = Brushes.White;
                lefBut.Background = Brushes.Green;
                stoBut.Background = Brushes.White;
            }
            
            _client.SendMessage("l", (int)SpeedSlider.Value);
            serverResponse = _client.GetResponse();
            AnalyzeMessage(serverResponse);
        }

        private void stoBut_Click(object sender, RoutedEventArgs e) //stop button function
        {
            string serverResponse;

            if ((int)SpeedSlider.Value > 0)
            {
                forBut.Background = Brushes.White;
                rigBut.Background = Brushes.White;
                revBut.Background = Brushes.White;
                lefBut.Background = Brushes.White;
                stoBut.Background = Brushes.Green;


                _client.SendMessage("s", (int)SpeedSlider.Value);
                serverResponse = _client.GetResponse();
                AnalyzeMessage(serverResponse);
            }
        }


        private void AnalyzeMessage(string message)
        {
            forBut.Background = Brushes.White;
            rigBut.Background = Brushes.White;
            revBut.Background = Brushes.White;
            lefBut.Background = Brushes.White;
            stoBut.Background = Brushes.White;
            string path = "C:\\Users\\משתמש\\Desktop\\Final Year Project\\team-1305-ori-zahavi\\GUI - ControlCommand\\Example-Images\\"; //path of all road signs images
            string[] roadsigns = { "Stop",
                                    "Speed limit (20km/h)",
                                    "Speed Limit 30 km/h",
                                    "Speed Limit 50 km/h",
                                    "Speed Limit 80 km/h",
                                    "No Passing",
                                    "Yield",
                                    "No Entry",
                                    "End of all speed limits",
                                    "Turn right",
                                     "Turn left",
                                     "Ahead only",
                                     "General caution"};

            for (int i = 0; i < roadsigns.Length; i++)
            {
                //if road sign has been detected - notify the user (display the road sign)
                if(message.Contains(roadsigns[i]))
                {
                    path += roadsigns[i] + ".png";
                    signsImage.Source = new BitmapImage(new Uri(path));
                    return;
                }
            }
        }
    }
}