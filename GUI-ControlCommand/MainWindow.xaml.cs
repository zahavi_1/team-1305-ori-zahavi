﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI_ControlCommand
{
    public partial class MainWindow : Window
    {
        private Communicator _client;
        public MainWindow()
        {
            this.WindowState = WindowState.Maximized;
            this.WindowStyle = WindowStyle.None;
            InitializeComponent();

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            
            try
            {                
                ConnectionDetails cd;
                cd.serverIp = Defines.IP;
                cd.serverPort = Defines.PORT;
                this._client = new Communicator(cd);
                this._client.SendMessage("HelloFromGUI", 0);
                ConnectionWindow nextWin = new ConnectionWindow(this._client);
                this.Close();
                nextWin.ShowDialog();
            }
            catch(Exception ex)
            {
                string message = "Unable to connect the server";
                string title = "Error";
                MessageBoxButton button = MessageBoxButton.OK;
                MessageBoxResult result = MessageBox.Show(message, title, button, MessageBoxImage.Error);
            }

        }


    }
}
