#include <SoftwareSerial.h>
#define LEFT_FORWARD 10
#define RIGHT_REVERSE 11
#define RIGHT_FORWARD 12
#define LEFT_REVERSE 13
#define MAX_LIMIT 50
#define RIGHT_TRIG_PIN A5
#define RIGHT_ECHO_PIN A4
#define LEFT_ECHO_PIN A3
#define LEFT_TRIG_PIN A2
#define DISTANCE_TO_STOP 12

SoftwareSerial cameraSerial(3, 5); //RX, TX

int distanceFromLeft = 20;
int distanceFromRight = 20;

typedef enum BreakOrder
{
  NoBreak = -1,
  FromBoth = 1,
  FromLeft = 2,
  FromRight = 3,
};

void setup() {
  //initial the ultrasonic sensors
  pinMode(RIGHT_TRIG_PIN, OUTPUT);
  pinMode(RIGHT_ECHO_PIN, INPUT);
  pinMode(LEFT_TRIG_PIN, OUTPUT);
  pinMode(LEFT_ECHO_PIN, INPUT);

  //initial motors
  pinMode(LEFT_FORWARD, OUTPUT);
  pinMode(LEFT_REVERSE, OUTPUT);
  pinMode(RIGHT_FORWARD, OUTPUT);
  pinMode(RIGHT_REVERSE, OUTPUT);

  //initial communicating streams with esp32 board
  cameraSerial.begin(9600);
  Serial.begin(115200);

}

void loop() {
  // put your main code here, to run repeatedly:
  if(cameraSerial.available())
  {
    String message = cameraSerial.readString();
    char flag = message[message.length() - 1];
    
  
    switch(flag)
    {
      case 'f':
        driveForward();
        break;
      case 'b':
        driveBack ();
        break;
      case 'l':
        driveLeft();
        break;
      case 'r':
        driveRight();
        break;
      case 's':
        stopCar();
        break;   
    }
  }
}


BreakOrder autoDrive()
{

  
  distanceFromLeft = distanceFromSensor(LEFT_TRIG_PIN, LEFT_ECHO_PIN);
  distanceFromRight = distanceFromSensor(RIGHT_TRIG_PIN, RIGHT_ECHO_PIN);
  
  if(distanceFromLeft <DISTANCE_TO_STOP && distanceFromRight < DISTANCE_TO_STOP)//if have obstacles from both sides
  {
    stopCar();
    
    driveBack();
    delay(1000);
    driveRight();
    delay(1500);
    return FromBoth; 
  }
  
  else if(distanceFromLeft < DISTANCE_TO_STOP)
  {
    stopCar();
    driveBack();
    delay(1000);
    driveRight();
    delay(1000);
    return FromLeft;
  }
  else if (distanceFromRight < DISTANCE_TO_STOP)
  {
    stopCar();
    driveBack();
    delay(1000);
    driveLeft();
    delay(1000);
    return FromRight;
  }
  //driveForward();
  return NoBreak; 
}

void driveForward()
{
  digitalWrite(LEFT_FORWARD, HIGH);
  digitalWrite(RIGHT_FORWARD,HIGH);
  digitalWrite(LEFT_REVERSE, LOW);
  digitalWrite(RIGHT_REVERSE, LOW);
}

void driveBack()
{
  digitalWrite(LEFT_FORWARD, LOW);
  digitalWrite(RIGHT_FORWARD, LOW);
  digitalWrite(LEFT_REVERSE, HIGH);
  digitalWrite(RIGHT_REVERSE, HIGH); 
}

void driveLeft()
{
  digitalWrite(LEFT_FORWARD, LOW);
  digitalWrite(RIGHT_FORWARD,HIGH );
  digitalWrite(LEFT_REVERSE, LOW);
  digitalWrite(RIGHT_REVERSE, LOW);
}
void driveRight()
{
   digitalWrite(LEFT_FORWARD, HIGH);
  digitalWrite(RIGHT_FORWARD, LOW);
  digitalWrite(LEFT_REVERSE, LOW);
  digitalWrite(RIGHT_REVERSE, LOW);
}

void stopCar()
{
  digitalWrite(LEFT_FORWARD, LOW);
  digitalWrite(RIGHT_FORWARD, LOW);
  digitalWrite(LEFT_REVERSE, LOW);
  digitalWrite(RIGHT_REVERSE, LOW);
}

int distanceFromSensor(int trigerPin, int echoPin)
{
  long duration;
  int distance;
  digitalWrite(trigerPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigerPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigerPin, LOW);
  
  //calculate distance from obstacle
  duration = pulseIn(echoPin, HIGH);
  distance = duration * 0.034 / 2;

  return distance;
}
