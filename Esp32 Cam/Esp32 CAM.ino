#include <WiFi.h>
#include <string.h>
#include "esp_camera.h"

#define PWDN_GPIO_NUM     32
#define RESET_GPIO_NUM    -1
#define XCLK_GPIO_NUM      0
#define SIOD_GPIO_NUM     26
#define SIOC_GPIO_NUM     27
#define Y9_GPIO_NUM       35
#define Y8_GPIO_NUM       34
#define Y7_GPIO_NUM       39
#define Y6_GPIO_NUM       36
#define Y5_GPIO_NUM       21
#define Y4_GPIO_NUM       19
#define Y3_GPIO_NUM       18
#define Y2_GPIO_NUM        5
#define VSYNC_GPIO_NUM    25
#define HREF_GPIO_NUM     23

#define PCLK_GPIO_NUM     22

#define MAX_CHAR_LIMITS 50

#define END_CONNECTION_MESSAGE "BYE"

//necessary identifiers to connect the server and WiFi local network
const char* ssid = "<WiFi network name>"; 
const char* password = "<WiFi password>";
const char* host = "<Server's ip in the subnet>";
const uint16_t port = 1221;

bool connectionEnds = false;

WiFiClient client;


void setup() 
{
  Serial.begin(9600);
  WiFi.begin(ssid, password);

  while(WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }
  


  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;

  if (psramFound()) {
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

  // Initializing Camera
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }

  if(!client.connect(host, port))
   {
      Serial.println("Connection to host failed, Exit program");
      connectionEnds = true;
   }
   else
   {
    client.print("hello from esp32");
   }
  
}

void loop() 
{
   char messageFromServer [MAX_CHAR_LIMITS] = "";
   
   if(connectionEnds) //end of conversation
   {
    client.stop();
    Serial.println("STOP");
    while(true);
   }
   
   //get server's response
   while(!client.available());
   Serial.println("Got here!");
   while (client.available())
   { 
      char c = client.read();
      strncat(messageFromServer, &c, 1);
   }
   
   //TODO -> SEND MESSAGE TO ARDUINO UNO
   Serial.write(messageFromServer);
   client.print("Got it!");
  /////////////////////////////////////////
   if(strcmp(messageFromServer, END_CONNECTION_MESSAGE) == 0 || !client.connected())
   {
    connectionEnds = true;
   }
     // capture camera frame
	camera_fb_t *fb = esp_camera_fb_get();
  if(!fb) {
     Serial.println("Camera capture failed");
      return;
  } else {
      Serial.println("Camera capture successful!");
  
  
  String message = "SIZE " + String(fb->len);
  client.print(message);
  const char *data = (const char *)fb->buf;

  client.write(data, fb->len);

   delay(500);
   
}
